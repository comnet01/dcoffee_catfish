/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.dao;

import com.Dcoffee.catfish.helper.DatabaseHelper;
import com.Dcoffee.catfish.model.CheckStoreDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fewxi
 */
public class CheckStoreDetailDao implements Dao<CheckStoreDetail>{

    @Override
    public CheckStoreDetail get(int id) {
        CheckStoreDetail check_detail = null;
        String sql = "SELECT * FROM check_store_detail WHERE check_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                check_detail = CheckStoreDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return check_detail;
    }


    public List<CheckStoreDetail> getAll() {
        ArrayList<CheckStoreDetail> list = new ArrayList();
        String sql = "SELECT * FROM check_store_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStoreDetail check_detail = CheckStoreDetail.fromRS(rs);
                list.add(check_detail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<CheckStoreDetail> getAll(String where, String order) {
        ArrayList<CheckStoreDetail> list = new ArrayList();
        String sql = "SELECT * FROM check_store_detail where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStoreDetail check_detail = CheckStoreDetail.fromRS(rs);
                list.add(check_detail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<CheckStoreDetail> getAll(String order) {
        ArrayList<CheckStoreDetail> list = new ArrayList();
        String sql = "SELECT * FROM check_store_detail ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStoreDetail check_detail = CheckStoreDetail.fromRS(rs);
                list.add(check_detail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
     public List<CheckStoreDetail> getAll(int id) {
        ArrayList<CheckStoreDetail> list = new ArrayList();
        String sql = "SELECT * FROM check_store_detail WHERE check_store_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                CheckStoreDetail item = CheckStoreDetail.fromRS(rs);
                list.add(item);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

//    @Override
//    public CheckStoreDetail save(CheckStoreDetail obj) {
//
//        String sql = "INSERT INTO check_store_detail (check_store_id , material_id, material_name, material_qty)"
//                + "VALUES(?, ?, ?, ?)";
//        
//        String sqlup = "UPDATE material SET material_unit = ? WHERE material_id = ?";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
//            stmt.setInt(1, obj.getCheckstoreId());
//            stmt.setInt(2, obj.getMaterialId());
//            stmt.setString(3, obj.getMaterialName());
//            stmt.setInt(4, obj.getMaterialQty());
////            System.out.println(stmt);
//            stmt.executeUpdate();
//            int id = DatabaseHelper.getInsertedId(stmt);
//            obj.setId(id);
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//            return null;
//        }
//        return obj;
//    }
     
    @Override
    public CheckStoreDetail save(CheckStoreDetail obj) {
    String sqlInsert = "INSERT INTO check_store_detail (check_store_id, material_id, material_name, material_qty) VALUES (?, ?, ?, ?)";
    String sqlUpdateMaterial = "UPDATE material SET material_qty = ? WHERE material_id = ?";
    
    Connection conn = DatabaseHelper.getConnect();
    
    try {
        // เพิ่มข้อมูลในตาราง check_store_detail
        PreparedStatement stmtInsert = conn.prepareStatement(sqlInsert, Statement.RETURN_GENERATED_KEYS);
        stmtInsert.setInt(1, obj.getCheckstoreId());
        stmtInsert.setInt(2, obj.getMaterialId());
        stmtInsert.setString(3, obj.getMaterialName());
        stmtInsert.setInt(4, obj.getMaterialQty());
        
        stmtInsert.executeUpdate();
        
        // รับคีย์หลักหลังการเพิ่ม
        ResultSet generatedKeys = stmtInsert.getGeneratedKeys();
        if (generatedKeys.next()) {
            int id = generatedKeys.getInt(1);
            obj.setId(id);
        }
        
        // อัปเดตข้อมูลในตาราง material
        PreparedStatement stmtUpdateMaterial = conn.prepareStatement(sqlUpdateMaterial);
        stmtUpdateMaterial.setInt(1, obj.getMaterialQty()); // อัปเดตจำนวนวัสดุ
        stmtUpdateMaterial.setInt(2, obj.getMaterialId());
        
        stmtUpdateMaterial.executeUpdate();
        
    } catch (SQLException ex) {
        System.out.println(ex.getMessage());
        return null;
    }
    
    return obj;
}



    @Override
    public CheckStoreDetail update(CheckStoreDetail obj) {
        String sql = "UPDATE check_store_detail"
                + " SET check_store_id = ?, material_id = ?, material_name = ?, material_qty = ?, "
                + " WHERE check_detail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getCheckstoreId());
            stmt.setInt(2, obj.getMaterialId());
            stmt.setString(3, obj.getMaterialName());
            stmt.setInt(4, obj.getMaterialQty());
            stmt.setInt(5, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckStoreDetail obj) {
        String sql = "DELETE FROM check_store_detail WHERE check_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

    

}
