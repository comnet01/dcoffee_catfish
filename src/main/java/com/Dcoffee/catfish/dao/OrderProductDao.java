/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.dao;

import com.Dcoffee.catfish.helper.DatabaseHelper;
import com.Dcoffee.catfish.model.OrderProduct;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Miso
 */
public class OrderProductDao implements Dao<OrderProduct> {

    @Override
    public OrderProduct get(int id) {
        OrderProduct item = null;
        String sql = "SELECT * FROM order_product WHERE order_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = OrderProduct.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }
    
    public List<OrderProduct> getAll(int id) {
        ArrayList<OrderProduct> list = new ArrayList();
        String sql = "SELECT * FROM order_product WHERE order_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                OrderProduct item = OrderProduct.fromRS(rs);
                list.add(item);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    

    public OrderProduct getLast() {
        OrderProduct item = null;
        String sql = "SELECT * FROM order_product ORDER BY order_product_id DESC LIMIT 1";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = OrderProduct.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    public List<OrderProduct> getAll() {
        ArrayList<OrderProduct> list = new ArrayList();
        String sql = "SELECT * FROM order_product";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderProduct item = OrderProduct.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<OrderProduct> getAll(String where, String order) {
        ArrayList<OrderProduct> list = new ArrayList();
        String sql = "SELECT * FROM order_product where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderProduct item = OrderProduct.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<OrderProduct> getAll(String order) {
        ArrayList<OrderProduct> list = new ArrayList();
        String sql = "SELECT * FROM order_product  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderProduct item = OrderProduct.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public OrderProduct save(OrderProduct obj) {

        String sql = "INSERT INTO order_product (order_product_id, order_id, product_name, product_size, product_type, product_level, amount, order_product_price, total)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getOrderProductId());
            stmt.setInt(2, obj.getOrderId());
            stmt.setString(3, obj.getProductName());
            stmt.setString(4, obj.getProductSize());
            stmt.setString(5, obj.getProductType());
            stmt.setString(6, obj.getProductSweetness());
            stmt.setInt(7, obj.getProductAmount());
            stmt.setDouble(8, obj.getProductPrice());
            stmt.setDouble(9, obj.getTotal());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setOrderProductId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public OrderProduct update(OrderProduct obj) {
        return null;
    }

    @Override
    public int delete(OrderProduct obj) {
        String sql = "DELETE FROM order_product WHERE order_product_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getOrderProductId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
    public int deleteByOrderID(int ID) {
        String sql = "DELETE FROM order_product WHERE order_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, ID);
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
