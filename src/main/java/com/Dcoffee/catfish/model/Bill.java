/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.crypto.Data;

/**
 *
 * @author User
 */
public class Bill {

    private int id;
    private Date billDate;
    private int total;
    private int billid;
    private int employeeid;
    private ArrayList<BillDetail> billDetail = new ArrayList();

    public Bill(int id, Date billDate, int total, int billid, int employeeid) {
        this.id = id;
        this.billDate = billDate;
        this.total = total;
        this.billid = billid;
        this.employeeid = employeeid;
    }
    
    public Bill(Date billDate, int total, int billid, int employeeid) {
        this.id = -1;
        this.billDate = billDate;
        this.total = total;
        this.billid = billid;
        this.employeeid = employeeid;
    }

    public Bill() {
        this.id = -1;
        this.billDate = null;
        this.total = 0;
        this.billid = 0;
        this.employeeid = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getBillid() {
        return billid;
    }

    public void setBillid(int billid) {
        this.billid = billid;
    }

    public int getEmployeeid() {
        return employeeid;
    }

    public void setEmployeeid(int employeeid) {
        this.employeeid = employeeid;
    }

    public ArrayList<BillDetail> getBillDetail() {
        return billDetail;
    }

    public void setBillDetail(ArrayList<BillDetail> billDetail) {
        this.billDetail = billDetail;
    }

    @Override
    public String toString() {
        return "Bill{" + "id=" + id + ", billDate=" + billDate + ", total=" + total + ", billid=" + billid + ", employeeid=" + employeeid + ", billDetail=" + billDetail + '}';
    }
    
    public static Bill fromRS(ResultSet rs) {
        Bill bill = new Bill();
        try {
            bill.setId(rs.getInt("id"));
            bill.setTotal(rs.getInt("total"));
            bill.setBillDate(rs.getTimestamp("bill_date"));
            bill.setBillid(rs.getInt("bill_id"));
            bill.setEmployeeid(rs.getInt("employee_id"));
        } catch (SQLException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return bill;
    }

    public void addBillDeial(Material material, int i) {
        BillDetail bd = new BillDetail(material.getMaterialID(), material.getMaterialName(), (int) material.getMaterialTotalPrice(), (int) material.getMaterialPrice(), material.getMaterialQty(), -1, (int) material.getMaterialTotalPrice());
        //BillDetail bd = new BillDetail(material.getMtrID(), material.getMtrName(), material.getMtrPrice());
        billDetail.add(bd);
        calculateTotal();
    }
    
    public void calculateTotal() {
        int totalQty = 0;
        float total = 0.0f;
        for(BillDetail bd: billDetail) {
            total += bd.getBillDetailTotailprice();
            totalQty += bd.getBillDetailQty();
        }
        //this.totalQty = totalQty;
        this.total = (int) total;
    }
}
