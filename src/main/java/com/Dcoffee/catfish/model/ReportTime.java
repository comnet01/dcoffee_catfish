/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miso
 */
public class ReportTime {

    private int employeeId;
    private String userName;
    private String attendanceStatus;
    private String checkIntime;
    private String checkInOut;
    private int totalwork;

    public ReportTime(int employeeId, String userName, String attendanceStatus, String checkIntime, String checkInOut, int totalwork) {
        this.employeeId = employeeId;
        this.userName = userName;
        this.attendanceStatus = attendanceStatus;
        this.checkIntime = checkIntime;
        this.checkInOut = checkInOut;
        this.totalwork = totalwork;
    }

    
    public ReportTime() {
        this(0,"","","","",0);
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAttendanceStatus() {
        return attendanceStatus;
    }

    public void setAttendanceStatus(String attendanceStatus) {
        this.attendanceStatus = attendanceStatus;
    }

    public String getCheckIntime() {
        return checkIntime;
    }

    public void setCheckIntime(String checkIntime) {
        this.checkIntime = checkIntime;
    }

    public String getCheckInOut() {
        return checkInOut;
    }

    public void setCheckInOut(String checkInOut) {
        this.checkInOut = checkInOut;
    }

    public int getTotalwork() {
        return totalwork;
    }

    public void setTotalwork(int totalwork) {
        this.totalwork = totalwork;
    }

    @Override
    public String toString() {
        return "ReportTime{" + "employeeId=" + employeeId + ", userName=" + userName + ", attendanceStatus=" + attendanceStatus + ", checkIntime=" + checkIntime + ", checkInOut=" + checkInOut + ", totalwork=" + totalwork + '}';
    }

    
    public static ReportTime fromRS(ResultSet rs) {
        ReportTime obj = new ReportTime();
        try {
            
            obj.setEmployeeId(rs.getInt("employee_id"));
            obj.setUserName(rs.getString("user_name"));
            obj.setAttendanceStatus(rs.getString("attendance_status"));
            obj.setCheckIntime(rs.getString("check_in_time"));
            obj.setCheckInOut(rs.getString("check_out_time"));
            obj.setTotalwork(rs.getInt("total_work_hours"));

        } catch (SQLException ex) {
            Logger.getLogger(ReportTime.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    } 

}
