/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miso
 */
public class ReportToday {

    private double totalSales;
    private int totalorders;

    public ReportToday(double totalSales, int totalorders) {
        this.totalSales = totalSales;
        this.totalorders = totalorders;
    }
     public ReportToday() {
        this(0,0);
    }

    public double getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(double totalSales) {
        this.totalSales = totalSales;
    }

    public int getTotalorders() {
        return totalorders;
    }

    public void setTotalorders(int totalorders) {
        this.totalorders = totalorders;
    }

    @Override
    public String toString() {
        return "ReportToday{" + "totalSales=" + totalSales + ", totalorders=" + totalorders + '}';
    }
     public static ReportToday fromRS(ResultSet rs) {
        ReportToday obj = new ReportToday();
        try {
            
            obj.setTotalSales(rs.getDouble("TotalSales"));
            obj.setTotalorders(rs.getInt("TotalOrders"));

        } catch (SQLException ex) {
            Logger.getLogger(ReportToday.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    }  
    

}
