/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import com.Dcoffee.catfish.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class StockImport {
    private int id;
    private LocalDate createdDate;
    private int userId;
    private int stockImportTotal;
    private User user;
    private ArrayList<StockImport> stockImport = new ArrayList<StockImport>();
    private float total;

    public StockImport(int id, LocalDate createdDate, int userId) {
        this.id = id;
        this.createdDate = createdDate;
        this.userId = userId;
    }
    
    public StockImport(LocalDate createdDate, int userId) {
        this.id = -1;
        this.createdDate = createdDate;
        this.userId = userId;
    }
    public StockImport() {
        this.id = 0;
        this.createdDate = createdDate;
        this.userId = 0;
    }

    public StockImport(int id, LocalDate createdDate, int userId, int stockImportTotal, User user) {
        this.id = id;
        this.createdDate = createdDate;
        this.userId = userId;
        this.stockImportTotal = stockImportTotal;
        this.user = user;
    }
    
    public StockImport(LocalDate createdDate, int userId, int stockImportTotal, User user) {
        this.id = -1;
        this.createdDate = createdDate;
        this.userId = userId;
        this.stockImportTotal = stockImportTotal;
        this.user = user;
    }
    
//    public StockImport() {
//        this.id = 0;
//        this.createdDate = createdDate;
//        this.userId = 0;
//        this.stockImportTotal = 0;
//        this.user = user;
//    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public ArrayList<StockImport> getStockImport() {
        return stockImport;
    }

    public void setStockImport(ArrayList<StockImport> stockImport) {
        this.stockImport = stockImport;
    }

    public int getStockImportTotal() {
        return stockImportTotal;
    }

    public void setStockImportTotal(int stockImportTotal) {
        this.stockImportTotal = stockImportTotal;
    }
//    public ArrayList<StockImport> getBillDetail() {
//        return stockImport;
//    }

    @Override
    public String toString() {
        return "StockImport{" + "id=" + id + ", createdDate=" + createdDate + ", userId=" + userId + ", stockImportTotal=" + stockImportTotal + ", user=" + user + ", stockImport=" + stockImport + '}';
    }
    
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user.getId();
    }
    
//    public void addHisMatDetail(StockImport stockImportDetail) {
//        stockImportDetails.add(stockImportDetail);
//    }
//
//    public void addHisMatDetail(StockImport material) {
//        StockImport md = new StockImport(material.getId(), material.get(), material.getName(), material.getUnit(),
//                material.getRemain(), -1);
//        stockImportDetails.add(md);
//    }
//
//    public void delHisMatDetail(StockImport stockImportDetail) {
//        stockImportDetails.remove(stockImportDetail);
//    }
    
//    public void add(Material material, int i) {
//        StockImportDetail std = new StockImportDetail(userId, material.getMaterialName(), material.getMaterialUnit(), "", userId, userId, userId, stockImportTotal);
//        //BillDetail bd = new BillDetail(material.getMtrID(), material.getMtrName(), material.getMtrPrice());
//        stockImport.add(std);
//        calculateTotal();
//    }
    
    public ArrayList<StockImport> getStockImportDetail() {
        return stockImport;
    }

    public static StockImport fromRS(ResultSet rs) {
        StockImport stockImport = new StockImport();
        try {
            stockImport.setId(rs.getInt("id"));
            stockImport.setCreatedDate(rs.getDate("stock_import_date").toLocalDate());
            stockImport.setUserId(rs.getInt("user_id"));
            stockImport.setStockImportTotal(rs.getInt("stock_import_total"));

//            Population
            UserDao userDao = new UserDao();
            User user = userDao.get(stockImport.getUserId());
            stockImport.setUser(user);
        } catch (SQLException ex) {
            Logger.getLogger(StockImport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return stockImport;
    }
    
    public void delStockImportDetail(StockImportDetail hisInvoice) {
        stockImport.remove(hisInvoice);
        calculateTotal();
    }

//    private void calculateTotal() {
//        int totalQty = 0;
//        float total = 0.0f;
//        for(StockImportDetail std: stockImport) {
//            total += std.getBillDetailTotailprice();
//            totalQty += std.getBillDetailQty();
//        }
//        //this.totalQty = totalQty;
//        this.total = (int) total;
//    }

    public void calculateTotal() {
        int totalQty = 0;
        float total = 0.0f;
        for (StockImport hd : stockImport) {
            total += hd.getStockImportTotal();
            //totalQty += hd.getMaterialQty();
        }
        //this.totalQty = totalQty;
        this.total = total;
    }
}
