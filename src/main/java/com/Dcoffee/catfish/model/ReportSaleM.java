/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miso
 */
public class ReportSaleM {
    private String mounth;
    private double totalSum;
    
    public ReportSaleM(String mounth,double totalSum) {
        this.mounth = mounth;
        this.totalSum = totalSum;
    }
    public ReportSaleM() {
        this("",0);
    }

    public String getMounth() {
        return mounth;
    }

    public void setMounth(String mounth) {
        this.mounth = mounth;
    }

    public double getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(double totalSum) {
        this.totalSum = totalSum;
    }

    @Override
    public String toString() {
        return "ReportSaleM{" + "mounth=" + mounth + ", totalSum=" + totalSum + '}';
    }
    public static ReportSaleM fromRS(ResultSet rs) {
        ReportSaleM obj = new ReportSaleM();
        try {
            
            obj.setMounth(rs.getString("Month"));
            obj.setTotalSum(rs.getInt("total_sum"));

        } catch (SQLException ex) {
            Logger.getLogger(ReportSaleM.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    } 
    
}
