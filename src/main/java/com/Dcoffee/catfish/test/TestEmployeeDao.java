/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.test;

import com.Dcoffee.catfish.dao.CustomerDao;
import com.Dcoffee.catfish.dao.EmployeeDao;
import com.Dcoffee.catfish.dao.UserDao;
import com.Dcoffee.catfish.helper.DatabaseHelper;
import com.Dcoffee.catfish.model.Customer;
import com.Dcoffee.catfish.model.Employee;
import com.Dcoffee.catfish.model.User;

/**
 *
 * @author HP
 */
public class TestEmployeeDao {

    public static void main(String[] args) {
        EmployeeDao employeeDao = new EmployeeDao();
        for (Employee u : employeeDao.getAll()) {
            System.out.println(u);
        }
//        System.out.println("________________________________");
//        //getone
//        Employee cus1 = employeeDao.get(1);
//        System.out.println(cus1);
//        System.out.println("________________________________");
//        for (Employee u : employeeDao.getAll()) {
//            System.out.println(u);
//        }
//
//        DatabaseHelper.close();
    }
}