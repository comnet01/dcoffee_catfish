/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.test;

import com.Dcoffee.catfish.model.Product;
import com.Dcoffee.catfish.service.ProductService;

/**
 *
 * @author STDG_077
 */
public class TestProductService {
    public static void main(String[] args) {
        ProductService cs =  new ProductService();
        for(Product product:cs.getProducts()) {
            System.out.println(product);
        }
    }
}
