/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.Dcoffee.catfish.ui;

import com.Dcoffee.catfish.dao.StockImportDao;
import com.Dcoffee.catfish.model.Bill;
import com.Dcoffee.catfish.model.BillDetail;
import com.Dcoffee.catfish.model.StockImportDetail;
import com.Dcoffee.catfish.model.Material;
import com.Dcoffee.catfish.model.StockImportDetail;
import com.Dcoffee.catfish.model.StockImport;
import com.Dcoffee.catfish.service.MaterialService;
import com.Dcoffee.catfish.service.StockImportDetailService;
import com.Dcoffee.catfish.service.StockImportService;
import com.Dcoffee.catfish.service.UserService;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author User
 */
public class StockImportP extends javax.swing.JPanel {
    //private List<StockImport> stockImport;
    //com.Dcoffee.catfish.model.StockImport stockImport;
    //private StockImportDetail editedStockImportDetail;

    private final JScrollPane scrMain;
    ArrayList<StockImport> inList;
    private List<StockImportDetail> stockImportList;
    private final StockImportDetailService stockImportDetailService = null;
    StockImportService stockImportService = new StockImportService();
    AbstractTableModel ttblImportModel;
    StockImportDao stockImportDao = new StockImportDao();
    UserService userService = new UserService();
    StockImport stockImport;
    Material material;
    MaterialService materialService;

//    private MaterialService materialService;
//    private List<Material> materialList;
//    StockImportService stockImportService = new StockImportService();
//    private StockImport StockImport;
//    private ArrayList<StockImportDetailService> stockImportDetail = new ArrayList<StockImportDetail>();
//    private final JScrollPane scrMain;
//    ArrayList<StockImport> stockImportList;
//    private List<StockImportDetail> detailList;
//    private final StockImportDetailService stockImportDetailService;
//    StockImportService stockImportService = new StockImportService();
//    AbstractTableModel tbstockImport;
//    StockImportDao stockImportDao = new StockImportDao();
//    UserService userService = new UserService();
//    StockImport stockImport;
//    Material mat;
//    MaterialService matService;
//    MaterialService materialService = new MaterialService();
//    StockImportDetailService stockImportDetailService = new StockImportDetailService();
//    StockImportDetail stockImportDetail;
//     
//    private final StockImportService stockImportService;
//    private List<StockImport> stockImport;
//    private StockImport editedStockImport;
//    
//    private final StockImportDetailService stockImportDetailService;
//    private List<StockImportDetail> stockImportDetail;
//    private StockImportDetail editedStockImportDetail;
//    Bill bill;
//    DefaultTableModel model;
//
//    List<Material> materials;
//    MaterialService materialService = new MaterialService();
//
//    StockImportDetailService stockImportDetailService = new StockImportDetailService();
//    StockImportDetail stockImportDetail;
//    private StockImportDetail editedStockImportDetail;
//
//    StockImportService stockImportService = new StockImportService();
//    StockImport stockImport;
//    private StockImport editedStockImport;
//
//    private Material editedMrt = new Material();
    /**
     * Creates new form StockImport
     */
//    StockImport stockImport;
    public StockImportP(JScrollPane scrMain) {
        initComponents();
        initHisTable();
        this.scrMain = scrMain;

//        stockImport = new StockImport();
//        stockImport.setU stockImport = stockImportService.getStockImportDetails();
//
//        stockImportDetailService = new StockImportDetailService();
//        stockImportDetail = stockImportDetailService.();
//        initMaterialTable();
//        stockImport = (List<StockImport>) new StockImport();
        ImageIcon icon = new ImageIcon("invoice.png");
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) ((57 * width) / height), 57, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
        //lblImage.setIcon(icon);

        ImageIcon iconback = new ImageIcon("./leftarrow.png");
        Image imageback = iconback.getImage();
        Image newImageback = imageback.getScaledInstance((int) ((15 * width) / height), 15, Image.SCALE_SMOOTH);
        iconback.setImage(newImageback);
        //btnBack.setIcon(iconback);

        stockImportDao = new StockImportDao();
        stockImportService = new StockImportService();
        userService = new UserService();

        stockImport = new StockImport();
        stockImportList = stockImportDetailService.getHistoryInvoiceDetails();

        tblImport.setRowHeight(30);
        tblImport.setModel(new AbstractTableModel() {
            String[] headers = {"Name", "Company", "Price", "qty", "total"};

            @Override
            public String getColumnName(int column) {
                return headers[column];  // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
            }

            @Override
            public int getRowCount() {
                return stockImportList.size();
            }

            @Override
            public int getColumnCount() {
                return 5;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                //ArrayList<StockImportDetail> stockImportDetails = stockImportList.get();
                StockImportDetail stockImportDetail = stockImportList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return stockImportDetail.getMaterialName();
                    case 1:
                        return stockImportDetail.getStockImportDetailCompany();
                    case 2:
                        return stockImportDetail.getMaterialPrice();
                    case 3:
                        return stockImportDetail.getMaterialQty();
                    case 4:
                        return stockImportDetail.getMaterialTotalPrice();
                    default:
                        return "";
                }
            }

        });

    }
//

    private void initHisTable() {
        inList = stockImportService.getStockImport();
        tblMaterial.setRowHeight(30);
        tblMaterial.setModel(new AbstractTableModel() {
            String[] hishead = {"Name", "Price", "qty", "total"};
            //String[] hishead = {"ID", "Employee", "Date", "Total"};

            @Override
            public String getColumnName(int column) {
                return hishead[column];
            }

            @Override
            public int getRowCount() {
                return stockImportList.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                //StockImportDetail stockImport = inList.get(rowIndex);
                switch (columnIndex) {
//                    case 0:
//                        return stockImport.getId();
//                    case 1:
//                        return stockImport.getUser().getName();
//                    case 2:
//                        return stockImport.getDate();
//                    case 3:
//                        return stockImport.getTotal();
//                    default:
//                        return "";

//                    case 0:
//                        return stockImport.getMaterialName();
//                    case 1:
//                        return stockImport.getMaterialPrice();
//                    case 2:
//                        return stockImport.getMaterialQty();
//                    case 3:
//                        return stockImport.getMaterialTotalPrice();
                    default:
                        return "";
                }
            }

        }
        );
    }
//    public StockImportP() {
//        initComponents();
//        initMaterialTable();
//        stockImport = new StockImport();
//        tblImport.setModel(new AbstractTableModel() {
//            String[] headers = {"Name", "Price", "qty", "total"};
//
//            @Override
//            public String getColumnName(int column) {
//                return headers[column];  // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
//            }
//
//            @Override
//            public int getRowCount() {
//                return stockImport.getStockImport().size();
//            }
//
//            @Override
//            public int getColumnCount() {
//                return 4;
//            }
//
//            @Override
//            public Object getValueAt(int rowIndex, int columnIndex) {
//                ArrayList<BillDetail> stockImportDetails = bill.getBillDetail();
//                BillDetail stockImportDetail = stockImportDetails.get(rowIndex);
//                switch (columnIndex) {
//                    case 0:
//                        return stockImportDetail.getMaterialName();
//                    case 1:
//                        return stockImportDetail.getBillDetailPriceperunit();
//                    case 2:
//                        return stockImportDetail.getBillDetailQty();
//                    case 3:
//                        return stockImportDetail.getBillDetailTotailprice();
//                    default:
//                        return "";
//                }
//            }
//
//        });
//    }
//
//    private void initMaterialTable() {
//        materials = materialService.getMaterials();
//        tblMaterial.setRowHeight(100);
//        tblMaterial.setModel(new AbstractTableModel() {
//            String[] headers = {"Image", "ID", "Name", "Price"};
//
//            @Override
//            public Class<?> getColumnClass(int columnIndex) {
//                switch (columnIndex) {
//                    case 0:
//                        return ImageIcon.class;
//                    default:
//                        return String.class;
//                }
//            }
//
//            @Override
//            public String getColumnName(int column) {
//                return headers[column]; // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
//            }
//
//            @Override
//            public int getRowCount() {
//                return materials.size();
//            }
//
//            @Override
//            public int getColumnCount() {
//                return 4;
//            }
//
//            @Override
//            public Object getValueAt(int rowIndex, int columnIndex) {
//
//                Material material = materials.get(rowIndex);
//                switch (columnIndex) {
//                    case 0:
//                        ImageIcon icon = new ImageIcon("img/material" + material.getMaterialID() + ".png");
//                        Image image = icon.getImage();
//                        int width = image.getWidth(null);
//                        int height = image.getHeight(null);
//                        Image newImage = image.getScaledInstance((int) ((width * 100.0) / height), 100, Image.SCALE_SMOOTH);
//                        icon.setImage(newImage);
//                        return icon;
//                    case 1:
//                        return material.getMaterialID();
//                    case 2:
//                        return material.getMaterialName();
//                    case 3:
//                        return material.getMaterialPrice();
//                    default:
//                        return "";
//                }
//            }
//
//        });
//        tblMaterial.addMouseListener(new MouseAdapter() {

//            @Override
//            public void mouseClicked(MouseEvent e) {
//                int row = tblMaterial.rowAtPoint(e.getPoint());
//                int col = tblMaterial.columnAtPoint(e.getPoint());
//                System.out.println(materials.get(row));
//                Material material = materials.get(row);
//                stockImport.add(material, 1);
//                refreshReciept();
//            }
//            int selectedIndex = tbHis.getSelectedRow();
//
//            if (selectedIndex >= 0) {
//            HistoryInvoiceModel him = inList.get(selectedIndex);
//
//                invoiceList = hisInvoiceDetailService.getHistoryInvoiceDetailsById(him.getId());
//                tbHisDetail.setModel(tbHisDetailModel);
//                refreshHisTable(him.getId());
//            }
//            private void refreshReciept() {
//                tblImport.repaint();
//                tblImport.revalidate();
//                tblMaterial.repaint();
//                tblMaterial.revalidate();
//                lblSum.setText("Sum:" + bill.getTotal());
//            }
//
//        });
//    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    //@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblMaterial = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblImport = new javax.swing.JTable();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        lblSum = new javax.swing.JLabel();

        tblMaterial.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblMaterial.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblMaterial);

        tblImport.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblImport.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblImport);

        btnSave.setText("save");

        btnCancel.setText("cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        lblSum.setText("sum: ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCancel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSave)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblSum, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSum, javax.swing.GroupLayout.DEFAULT_SIZE, 47, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSave)
                    .addComponent(btnCancel))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
//        enablefrom(true);
//        if (isTableisEmpty() == false || stockImport != null) {
//            int result = JOptionPane.showConfirmDialog(null, "Want to cancel ?", "Confirm cancellation",
//                    JOptionPane.YES_NO_OPTION,
//                    JOptionPane.QUESTION_MESSAGE);
//            if (result == JOptionPane.YES_OPTION) {
//                enablefrom(false);
//                clear();
//            }
//        } else {
//            JFrame f = new JFrame();
//            JOptionPane.showMessageDialog(f, "Unable to cancel current bill. Because the item is still empty.", "Unable to cancel bill", JOptionPane.INFORMATION_MESSAGE);
//        }
//        if (isTableisEmpty() == false || stockImport != null) {
//            int result = JOptionPane.showConfirmDialog(null, "Want to cancel ?", "Confirm cancellation",
//                    JOptionPane.YES_NO_OPTION,
//                    JOptionPane.QUESTION_MESSAGE);
//            if (result == JOptionPane.YES_OPTION) {
//                enablefrom(false);
//                clear();
//            }
//        } else {
//            JFrame f = new JFrame();
//            JOptionPane.showMessageDialog(f, "Unable to cancel current bill. Because the item is still empty.", "Unable to cancel bill", JOptionPane.INFORMATION_MESSAGE);
//        }
    }//GEN-LAST:event_btnCancelActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnSave;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblSum;
    private javax.swing.JTable tblImport;
    private javax.swing.JTable tblMaterial;
    // End of variables declaration//GEN-END:variables

//    private void enablefrom(boolean status) {
//        DefaultTableModel model = new DefaultTableModel();
//        this.model = model; // กำหนดค่าให้กับ this.model
//
//        if (status == false) {
//            model.setRowCount(0);
//            lblSum.setText("Sum: 0.00");
//        }
//        lblSum.setEnabled(status);
//        btnCancel.setEnabled(status);
//        btnSave.setEnabled(status);
//        tblMaterial.setEnabled(status);
//        tblImport.setEnabled(status);
//    }
//    private boolean isTableisEmpty() {
//        return model.getRowCount() <= 0;
//    }
//
//    private void clear() {
//        DefaultTableModel model = new DefaultTableModel();
//        tblImport.setModel(model);
//    }
}
