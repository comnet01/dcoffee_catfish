/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.Dcoffee.catfish.ui;

import com.Dcoffee.catfish.dao.StockImportDao;
import com.Dcoffee.catfish.model.Material;
import com.Dcoffee.catfish.model.StockImport;
import com.Dcoffee.catfish.model.StockImportDetail;
import com.Dcoffee.catfish.service.StockImportDetailService;
import com.Dcoffee.catfish.service.StockImportService;
import com.Dcoffee.catfish.service.UserService;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author User
 */
public class HistoryMaterial extends javax.swing.JPanel {

    //private final JScrollPane scrMain;
    ArrayList<StockImport> list;
    StockImportDao stockImportDao = new StockImportDao();
    StockImportService stockImportService = new StockImportService();
    UserService userService = new UserService();
    Material material;
    StockImportDetail stockImportDetail;
    AbstractTableModel tblMaterialDetailModel;
    private final StockImportDetailService hisMatDetailService;
    private List<StockImportDetail> matlist;
//    private final AbstractTableModel tbMatDetailModl;

    /**
     * Creates new form HistoryMaterial
     */
    public HistoryMaterial() {
        initComponents();
        //this.scrMain = scrMain;
        ImageIcon icon = new ImageIcon("./historymaterial.png");
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) ((57 * width) / height), 57, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
        //lblImage.setIcon(icon);

        ImageIcon iconback = new ImageIcon("./leftarrow.png");
        Image imageback = iconback.getImage();
        Image newImageback = imageback.getScaledInstance((int) ((15 * width) / height), 15, Image.SCALE_SMOOTH);
        iconback.setImage(newImageback);
        //btnBack.setIcon(iconback);

        stockImportDao = new StockImportDao();
        stockImportService = new StockImportService();
        userService = new UserService();
        hisMatDetailService = new StockImportDetailService();

        list = stockImportService.getStockImport();
        tblMaterial.setRowHeight(30);
        tblMaterial.setModel(new AbstractTableModel() {
            String[] header = {"ID", "Employee", "Date"};

            @Override
            public String getColumnName(int column) {
                return header[column];
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                StockImport hisMaterial = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return hisMaterial.getId();
                    case 1:
                        //return hisMaterial.getUser().getEmployee().getName();
                        return hisMaterial.getUser().getName();
                    case 2:
                        return hisMaterial.getCreatedDate();
                    default:
                        return "";
                }
            }

        });
        
        matlist = hisMatDetailService.getHistoryInvoiceDetails();
        tblMaterialDetail.setRowHeight(30);
        tblMaterialDetailModel = new AbstractTableModel() {
            String[] header = {"Key", "Name", "Remain", "Unit"};

            @Override
            public String getColumnName(int column) {
                return header[column];
            }

            @Override
            public int getRowCount() {
                return matlist.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                StockImportDetail hisMatDetailModel = matlist.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return hisMatDetailModel.getMaterialName();
                    case 1:
                        return hisMatDetailModel.getMaterialQty();
                    case 2:
                        return hisMatDetailModel.getMaterialUnit();
                    default:
                        return " ";
                }
            }
        };

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblMaterial = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblMaterialDetail = new javax.swing.JTable();
        btnAdd = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();

        tblMaterial.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblMaterial);

        tblMaterialDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblMaterialDetail);

        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnAdd)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDelete)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd)
                    .addComponent(btnDelete))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
//        scrMain.setViewportView(new AddStockImport(scrMain));
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedIndex = tblMaterial.getSelectedRow();
        if (selectedIndex >= 0) {
            //material = list.get(selectedIndex);
            int input = JOptionPane.showConfirmDialog(this,
                    "Do you want to Delete?", "Caution!!", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                //stockImportService.delete(material);
            }
            refreshTable();
        }
    }//GEN-LAST:event_btnDeleteActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnDelete;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblMaterial;
    private javax.swing.JTable tblMaterialDetail;
    // End of variables declaration//GEN-END:variables

    private void refreshTable() {
        list = stockImportService.getStockImport();
        tblMaterial.revalidate();
        tblMaterial.repaint();
    }
}
